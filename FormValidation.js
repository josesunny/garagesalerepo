var BottleTotalPrice = 0;
var CapTotalPrice = 0;
var PenTotalPrice = 0;
var CandyTotalPrice = 0;
var CupCakeTotalPrice = 0;
var ItemTotal = 0;
var Donation = 0;
var BottleCount = 0;
var CapCount = 0;
var PenCount = 0;
var CandyBagCount = 0;
var CupCakeCount = 0;
var UserName;
var Email;
var CardNumber;
var CardExpiryMonth;
var CardExpiryYear;
var lastFourDigits

function myFunction() {


    var EmailRegex = /^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@{[a-zA-Z0-9_\-\.]+0\.([a-zA-Z]{2,5}){1,25})+)*$/;
    var YearRegex = /^[12][0-9]{3}$/;
    var NameRegex = /^[A-Za-z\s]+$/;
    var CardNumberRegex = /^\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d$/;
    var MonthRegex = /^[A-Z][a-z][a-z]$/;

    event.preventDefault();
    UserName = document.getElementById("frmGarageSale").elements[0].value;
    Email = document.getElementById("frmGarageSale").elements[1].value;
    CardNumber = document.getElementById("frmGarageSale").elements[2].value;
    CardExpiryMonth = document.getElementById("frmGarageSale").elements[3].value;
    CardExpiryYear = document.getElementById("frmGarageSale").elements[4].value;
    BottleCount = document.getElementById("frmGarageSale").elements[5].value;
    CapCount = document.getElementById("frmGarageSale").elements[6].value;
    PenCount = document.getElementById("frmGarageSale").elements[7].value;
    CandyBagCount = document.getElementById("frmGarageSale").elements[8].value;
    CupCakeCount = document.getElementById("frmGarageSale").elements[8].value;

    var lastFourDigits = CardNumber.substr(-4)
    lastFourDigits = 'xxxx-xxxx-xxxx-' + lastFourDigits;


    validate();

    function calculate() {
        BottleTotalPrice = 5 * BottleCount;
        CapTotalPrice = 20 * CapCount;
        PenTotalPrice = 2 * PenCount;
        CandyTotalPrice = 10 * CandyBagCount
        CupCakeTotalPrice = 3 * CupCakeCount;
        ItemTotal = BottleTotalPrice + CapTotalPrice + PenTotalPrice + CandyTotalPrice + CupCakeTotalPrice;
        //alert(ItemTotal);
        if (ItemTotal > 10) {
            Donation = .10 * ItemTotal;
            ItemTotal = ItemTotal + Donation;
        }

        else {
            Donation = 10;
            ItemTotal = ItemTotal + Donation;
        }
        // alert("Donation ="+Donation);
        //alert("Total ="+ItemTotal);

        display();

    }

    function validate() {
        var x1 = document.getElementById("Error1");
        var x2 = document.getElementById("Error2");
        var x3 = document.getElementById("Error3");
        var x4 = document.getElementById("Error4");
        var x5 = document.getElementById("Error5");
        var x6 = document.getElementById("Error6");
        var x7 = document.getElementById("Error7");
        var x8 = document.getElementById("Error8");
        var x9 = document.getElementById("Error9");
        var x10 = document.getElementById("Error10");

        if (NameRegex.test(UserName) == false) {

            document.getElementById("Error1").innerHTML = "Enter valid name";

        }

        else {
            x1.style.display = "none";
        }
        if (EmailRegex.test(Email) == false) {

            document.getElementById("Error2").innerHTML = "Enter valid email";

        }
        else {
            x2.style.display = "none";
        }

        if (CardNumberRegex.test(CardNumber) == false) {

            document.getElementById("Error3").innerHTML = "Enter valid card number";
        }
        else {
            x3.style.display = "none";
        }

        if (MonthRegex.test(CardExpiryMonth) == false) {

            document.getElementById("Error4").innerHTML = "Enter valid expiry month";
        }

        else {
            x4.style.display = "none";
        }

        if (YearRegex.test(CardExpiryYear) == false) {

            document.getElementById("Error5").innerHTML = "Enter valid year";
        }
        else {
            x5.style.display = "none";
        }

        if (isNaN(parseInt(BottleCount)) == true) {

            document.getElementById("Error6").innerHTML = "Enter valid number";

        }
        else {
            x6.style.display = "none";
        }

        if (isNaN(parseInt(CapCount)) == true) {

            document.getElementById("Error7").innerHTML = "Enter valid number";
        }
        else {
            x7.style.display = "none";
        }

        if (isNaN(parseInt(PenCount)) == true) {

            document.getElementById("Error8").innerHTML = "Enter valid number";
        }
        else {
            x8.style.display = "none";
        }

        if (isNaN(parseInt(CandyBagCount)) == true) {

            document.getElementById("Error9").innerHTML = "Enter valid number";
        }
        else {
            x9.style.display = "none";
        }

        if (isNaN(parseInt(CupCakeCount)) == true) {

            document.getElementById("Error10").innerHTML = "Enter valid number";
        }
        else {
            x10.style.display = "none";
        }


        if ((UserName != "") && (Email != "") && (BottleCount + CapCount + PenCount + CandyBagCount + CupCakeCount != 0)) {

            x1.style.display = "none";
            x2.style.display = "none";
            x3.style.display = "none";
            x4.style.display = "none";
            x5.style.display = "none";
            x6.style.display = "none";
            x7.style.display = "none";
            x8.style.display = "none";
            x9.style.display = "none";
            x10.style.display = "none";
            calculate();
        }

    }


    function display() {

        const finalPrice = [
            { 'Product': 'Water Bottle', 'ProductCount': BottleCount, 'UnitPrice': '$5', 'TotalPrice': BottleTotalPrice },
            { 'Product': 'Cap', 'ProductCount': CapCount, 'UnitPrice': '$20', 'TotalPrice': CapTotalPrice },
            { 'Product': 'Pen', 'ProductCount': PenCount, 'UnitPrice': '$2', 'TotalPrice': PenTotalPrice },
            { 'Product': 'Candy Bag', 'ProductCount': CandyBagCount, 'UnitPrice': '$10', 'TotalPrice': CandyTotalPrice },
            { 'Product': 'Cup Cake', 'ProductCount': CupCakeCount, 'UnitPrice': '$3', 'TotalPrice': CupCakeTotalPrice }];

        items_table = document.getElementById('myTable');

        items_table.innerHTML += '<tr><td>' + 'Name ' + '</td>' + '<td colspan="3">' + UserName + '</td>' + '</td></tr>';
        items_table.innerHTML += '<tr><td>' + 'Email ' + '</td>' + '<td colspan="3">' + Email + '</td>' + '</td></tr>';
        items_table.innerHTML += '<tr><td>' + 'Credit Card Number ' + '</td>' + '<td colspan="3">' + lastFourDigits + '</td>' + '</td></tr>';
        items_table.innerHTML += '<tr><th>' + 'Item ' + '</th>' + '<th>' + 'Quantity' + '</th>' + '<th>' + 'Unit Price' + '</th>' + '<th>' + 'Total Price' + '</th>' + '</tr>';
        for (var ctr = 0; ctr < 5; ctr++) {

            while (finalPrice[ctr].TotalPrice != 0) {

                items_table.innerHTML += '<tr><td>' + finalPrice[ctr].Product + '</td>' + '<td>' + finalPrice[ctr].ProductCount + '</td>' + '<td>' + finalPrice[ctr].UnitPrice + '</td>' + '<td>' + '$' + finalPrice[ctr].TotalPrice + '</td></tr>';
                break;
            }
        }
        items_table.innerHTML += '<tr><td colspan="2">' + ' Donation ' + '</td>' + '<td colspan="2">' + '$' + Donation + '</td>' + '</td></tr>';
        items_table.innerHTML += '<tr><td colspan="2">' + 'Total' + '</td>' + '<td colspan="2">' + '$' + ItemTotal + '</td></tr>';

    }

}
